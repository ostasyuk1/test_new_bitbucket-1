version = "v1" 


policy "opa1" {
  enabled = false  
  enforcement_level = soft-mandatory
}
policy "opa2" {
  enabled = true  
  enforcement_level = soft-mandatory
}

policy "opa" {
  enforcement_level = advisory
}